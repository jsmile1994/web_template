import React, { Component } from 'react';
import './ProductList.css';

class ProductList extends Component {
    render() {
        return (
            <div className="panel panel-primary align-center">
                <div className="panel-heading">
                    <h3 className="panel-title">Danh sách sản phẩm</h3>
                </div>
                <div className="panel-body">

                    <table className="table align-center table-bordered table-hover">
                        <thead >
                            <tr>
                                <th className="align-center">STT</th>
                                <th className="align-center">Mã sản phẩm</th>
                                <th className="align-center">Tên</th>
                                <th className="align-center">Giá</th>
                                <th className="align-center">Trạng thái</th>
                                <th className="align-center">Thao tác</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.props.children}
                        </tbody>
                    </table>

                </div>
            </div>
        );
    }
}

export default ProductList;
