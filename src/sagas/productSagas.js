import * as Types from '../constants/ActionTypes';

import { put, takeLatest, takeEvery } from 'redux-saga/effects';
import { Api } from '../utils/Api';

function* fetchProducts() {
    try {
        const products = yield Api.getProductsFromApi();
        // console.log('âs');
        yield put({
            type: Types.FETCH_PRODUCTS_SUCCESS,
            products
        });
    } catch (error) {
        yield put({
            type: Types.FETCH_PRODUCTS_FAIL,
            error
        });
    }
}

function* addNewProduct(action) {
    try {
        const result = yield Api.insertNewProductFromApi(action.product);
        if (result === true) {
            yield put({
                type: Types.FETCH_PRODUCTS
            });
        }
    } catch (error) {

    }
}

function* deleteProduct(action) {
    try {
        const result = yield Api.deleteProductFromApi(action.id);
        if (result === true) {
            yield put({
                type: Types.DELETE_PRODUCT_SUCCESS,
                id: action.id
            });
        }
    } catch (error) {

    }
}

function* getProduct(action) {
    try {
        const product = yield Api.getProductFromApi(action.id);
        yield put({
            type: Types.FETCH_PRODUCT_SUCCESS,
            product
        });
    } catch (error) {
        yield put({
            type: Types.FETCH_PRODUCT_FAIL,
        });
    }
}

function* updateProduct(action) {
    try {
        const result = yield Api.updateProductFromApi(action.product);
        if (result === true) {
            yield put({
                type: Types.UPDATE_PRODUCT_SUCCESS,
                product: action.product
            });
        }
    } catch (error) {
    }
}

export function* watchFetchProducts() {
    yield takeEvery(Types.FETCH_PRODUCTS, fetchProducts);
}

export function* watchAddNewProduct() {
    yield takeLatest(Types.ADD_PRODUCT, addNewProduct);
}

export function* watchDeleteProduct() {
    yield takeLatest(Types.DELETE_PRODUCT, deleteProduct);
}

export function* watchFetchProduct() {
    yield takeLatest(Types.EDIT_PRODUCT, getProduct);
}

export function* watchUpdateProduct() {
    yield takeLatest(Types.UPDATE_PRODUCT, updateProduct);
}