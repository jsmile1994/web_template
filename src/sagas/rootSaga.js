import { fork } from 'redux-saga/effects';
import { watchAddNewProduct, watchDeleteProduct, watchFetchProducts, watchFetchProduct, watchUpdateProduct } from './productSagas';

export default function* rootSaga() {
    yield [
        fork(watchFetchProducts),
        fork(watchAddNewProduct),
        fork(watchDeleteProduct),
        fork(watchFetchProduct),
        fork(watchUpdateProduct)
    ];
}
