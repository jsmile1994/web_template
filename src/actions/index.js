import * as Types from './../constants/ActionTypes';

export const actFetchProducts = () => {
    return {
        type: Types.FETCH_PRODUCTS,
    }
}

export const actFetchProductsSuccess = (products) => {
    return {
        type: Types.FETCH_PRODUCTS_SUCCESS,
        products
    }
}
export const actFetchProductsFail = (error) => {
    return {
        type: Types.FETCH_PRODUCTS_FAIL,
        error
    }
}

export const actAddProduct = (product) => {
    return {
        type: Types.ADD_PRODUCT,
        product
    }
}

export const actDeleteProduct = (id) => {
    return {
        type: Types.DELETE_PRODUCT,
        id
    }
}

export const actDeleteProductSuccess = (id) => {
    return ({
        type: Types.DELETE_PRODUCT_SUCCESS,
        id
    });
}

export const actGetProduct = (id) => {
    return {
        type: Types.EDIT_PRODUCT,
        id
    }
}

export const actUpdateProduct = (product) => {
    return {
        type: Types.UPDATE_PRODUCT,
        product
    }
}

