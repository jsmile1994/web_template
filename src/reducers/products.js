import * as Types from '../constants/ActionTypes';
var INITIAL = [];

const products = (state = INITIAL, action) => {
    switch (action.type) {
        case Types.FETCH_PRODUCTS_SUCCESS:
            return [...action.products];
        case Types.FETCH_PRODUCTS_FAIL:
            return [];
        case Types.DELETE_PRODUCT_SUCCESS:
            const filteredMovies = [...state.filter(item => item.id !== action.id)];
            return filteredMovies;
        case Types.UPDATE_PRODUCT_SUCCESS:
            return state.map(product => {
                return (
                    (product.id === action.product.id)
                        ? {
                            ...product,
                            name: action.product.name,
                            price: action.product.price,
                            status: action.product.status
                        }
                        : product
                )
            });
        default:
            return [...state];
    }
};

export default products;