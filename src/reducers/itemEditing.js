import * as Types from '../constants/ActionTypes';

var INITIAL = {};

const itemEditing = (state = INITIAL, action) => {
    switch (action.type) {
        case Types.FETCH_PRODUCT_SUCCESS:
            return action.product;
        case Types.FETCH_PRODUCT_FAIL:
            return {};
        default:
            return { ...state };
    }
}

export default itemEditing;