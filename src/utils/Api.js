import callApi from './apiCaller';
import { all } from 'redux-saga/effects';

function* getProductsFromApi() {
    const response = yield callApi('products', 'GET', null);
    const products = yield response.status === 200 ? all(response.data) : [];
    return products;
}

function* insertNewProductFromApi(product) {
    const response = yield callApi('products', 'POST', product);
    return yield (response.status === 201);
}

function* deleteProductFromApi(id) {
    const response = yield callApi(`products/${id}`, 'DELETE', null);
    return yield (response.status === 200);
}

function* getProductFromApi(id) {
    const response = yield callApi(`products/${id}`, 'GET', null);
    const product = yield response.status === 200 ? all(response.data) : {};
    return product;
}

function* updateProductFromApi(product) {
    const response = yield callApi(`products/${product.id}`, 'PUT', product);
    return yield (response.status === 200);
}

export const Api = {
    getProductsFromApi,
    insertNewProductFromApi,
    deleteProductFromApi,
    getProductFromApi,
    updateProductFromApi
}