import React, { Component } from 'react';
import { connect } from 'react-redux';
// import { actAddProductRequest, actGetProductRequest, actUpdateProductRequest } from '../../actions';
import { actAddProduct, actGetProduct, actUpdateProduct } from '../../actions';

class ProductActionPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            id: '',
            txtName: '',
            txtPrice: '',
            cbStatus: ''
        }
    }

    componentDidMount() {
        var { match } = this.props;
        if (match) {
            var id = match.params.id;
            this.props.onEditProduct(id);
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps && nextProps.itemEditing) {
            var { itemEditing } = nextProps;
            this.setState({
                id: itemEditing.id,
                txtName: itemEditing.name,
                txtPrice: itemEditing.price,
                cbStatus: itemEditing.status
            });
        }
    }

    onChange = (e) => {
        var target = e.target;
        var name = target.name;
        var value = target.type === 'checkbox' ? target.checked : target.value;
        this.setState({
            [name]: value
        });
    }

    onSave = (e) => {
        e.preventDefault();
        var { id, txtName, txtPrice, cbStatus } = this.state;
        var { history } = this.props;
        let product = {
            id: id,
            name: txtName,
            price: txtPrice,
            status: cbStatus
        }
        if (id) {
            this.props.onUpdateProduct(product);
        } else {
            this.props.onAddProduct(product);
        }
        history.goBack();
    }

    render() {
        var { txtName, txtPrice, cbStatus } = this.state;
        return (
            <div className="row">
                <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <form onSubmit={this.onSave}>
                        <legend>Thêm sản phẩm mới</legend>
                        <div className="form-group">
                            <label>Tên sản phẩm</label>
                            <input
                                type="text"
                                className="form-control"
                                placeholder="Input field"
                                name="txtName"
                                value={txtName}
                                onChange={this.onChange}
                            />
                        </div>
                        <div className="form-group">
                            <label>Giá sản phẩm</label>
                            <input
                                type="number"
                                className="form-control"
                                placeholder="Input field"
                                name="txtPrice"
                                value={txtPrice}
                                onChange={this.onChange}
                            />
                        </div>
                        <div className="form-group">
                            <label>Trạng thái</label>
                        </div>

                        <div className="checkbox">
                            <label>
                                <input
                                    type="checkbox"
                                    value={cbStatus}
                                    name="cbStatus"
                                    onChange={this.onChange}
                                    checked={cbStatus}
                                />
                                Còn hàng
                            </label>
                        </div>

                        <button type="submit" className="btn btn-primary">Lưu lại</button>
                    </form>

                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        itemEditing: state.itemEditing
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        onAddProduct: (product) => {
            dispatch(actAddProduct(product));
        },
        onEditProduct: (id) => {
            dispatch(actGetProduct(id));
        },
        onUpdateProduct: (product) => {
            dispatch(actUpdateProduct(product));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductActionPage);
